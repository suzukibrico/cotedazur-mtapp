<mt:Include module="config" />
mtappVars.cotedazur = {
  courseBlogId: <mt:Var name="blog_id_course" />,
  freedrinkBlogId: <mt:Var name="blog_id_freedrink" />,
  optionBlogId: <mt:Var name="blog_id_option" />,
  storeBlogId: <mt:Var name="blog_id_store" />,
  formBlogId: <mt:Var name="blog_id_form" />
};
mtappVars.cotedazur.targetBlogIds = [];
for (var key in mtappVars.cotedazur) {
    mtappVars.cotedazur.targetBlogIds.push(mtappVars.cotedazur[key]);
}

// ラベルの変更（ RenameBlogLabelプラグインの動作と合わせるため特定のーザー限定にしない）
(function($){
  if (mtappVars.blog_id != <mt:Var name="blog_id_store" />) {
    return false;
  }
    $('h1,span').filter(':contains("カテゴリ")').each(function(){
        var html = $(this).html().replace('カテゴリ', '都道府県');
        $(this).html(html);
    });
    $('input[name="category_label"]').attr('placeholder', '都道府県名');
})(jQuery);
// ラベルの変更（ RenameBlogLabelプラグインの動作と合わせるため特定のーザー限定にしない）
(function($){
  if (mtappVars.blog_id != <mt:Var name="blog_id_course" />) {
    return false;
  }
    $('h1,span').filter(':contains("カテゴリ")').each(function(){
        var html = $(this).html().replace('カテゴリ', 'パターン');
        $(this).html(html);
    });
    $('input[name="category_label"]').attr('placeholder', 'パターン名');
})(jQuery);

// カテゴリ選択をラジをボタンにする
mtappVars.cotedazur.radioCategory = function(){
  jQuery.MTAppOtherTypeCategories({
    type: 'radio',
    label: 'カテゴリ',
    notSelectedText: '未選択',
    debug: false
  });
};

/* ==================================================
    共通
================================================== */
(function($){

  if ($.inArray(mtappVars.blog_id, mtappVars.cotedazur.targetBlogIds) === -1) {
    return false;
  }

  // body にブログを限定するクラスを追加
  $('body').addClass('blog-' + mtappVars.blog_id + ' active-userjs ' + mtappVars.screen_id);

  // 特定のユーザーだけメニューの表示・非表示をカスタマイズ
  if (mtappVars.author_id == 2  ||
      mtappVars.author_id == 11 ||
      mtappVars.author_id == 12 ||
      mtappVars.author_id == 13 ||
      mtappVars.author_id == 14 ) {

      $('body').addClass('customized-menu');
  }

  // グループの編集画面だけユーザー情報が正しくないバグ（？）があるので、そこだけユーザー名で判定する
  if (mtappVars.screen_id === 'edit-entrygroup' && 
      ($('#user span.username').text() === 'VALIC中村' ||
       $('#user span.username').text() === 'VALIC角田' ||
       $('#user span.username').text() === 'VALIC山中' ||
       $('#user span.username').text() === 'VALIC佐藤' ||
       $('#user span.username').text() === 'VALIC小椋' )
      ) {
    $('body').addClass('customized-menu');
  }

  // カスタマイズしたメニューの最後の要素のスタイルを調整
  $('#blog-wide-menu > .top-menu')
    .filter('.last-child')
    .removeClass('last-child')
    .end()
    .filter(':visible:last')
    .addClass('last-child');

  var cotedazurMaster = false;
  var cotedazurWriter = false;
  // ロールに関する変数をセットし、body にロールに関するクラスを追加
  if ($.inArray('コートダジュールブログ管理者', mtappVars.author_roles) !== -1) {
      cotedazurMaster = true;
      $('body').addClass('cotedazur-master');
  }
  if ($.inArray('コートダジュール投稿者', mtappVars.author_roles) !== -1) {
      cotedazurWriter = true;
      $('body').addClass('cotedazur-writer');
  }

  // 記事編集画面のドラッグ&ドロップを無効にする
  $('#sortable').sortable({ disabled: true });

})(jQuery);
/*  共通  */

/* ==================================================
    プランマトリクス管理：コース
================================================== */
(function($){

  if (mtappVars.blog_id != <mt:Var name="blog_id_course" />) {
    return false;
  }

  // フィールドの並べ替え
  var sort = [
    'title',
    'customfield_price',
    'customfield_under_time',
    'customfield_human_num',
    'customfield_child_num',
    'customfield_human_num_kanri',
    'customfield_child_num_kanri',
    'customfield_start_time',
    'customfield_end_time',
    'customfield_week',
    'customfield_start_date',
    'customfield_end_date',
    'customfield_freedrink_plan',
    'customfield_freedrink_plan_select',
    'customfield_course_img',
    'customfield_birthday_svc',
    'customfield_discount',
    'customfield_discount_date'
  ];
  $.MTAppFieldSort({
    sort: sort.join(','),
    otherFieldHide: true
  });

  // カテゴリ選択をラジをボタンにする
  //mtappVars.cotedazur.radioCategory();

  // MTAppListing のベースになるオプション設定
  var listingBaseOption = {
    url: '<mt:CGIRelativeURL /><mt:Var name="config.DataAPIScript" />/v3/sites/' + mtappVars.cotedazur.freedrinkBlogId + '/entries',
    data: {
      fields: 'id,title',
      limit: 9999999
    },
    jsontable: {
      header: {
        id: 'ID',
        title: 'タイトル'
      },
      headerOrder: ['id', 'title'],
      itemsRootKey: 'items',
      listingCheckboxType: 'checkbox',
      listingTargetKey: 'id',
      listingTargetEscape: false
    }
  };

  // 利用可能飲み放題
  var freedrinkPlanOption = $.extend({}, listingBaseOption, {});
  var $customfield_freedrink_plan = $('#customfield_freedrink_plan');
  $customfield_freedrink_plan
    .MTAppShowListEntries({
      api: mtappVars.DataAPI,
      siteId: mtappVars.cotedazur.freedrinkBlogId
    });
  $customfield_freedrink_plan
    .trigger('showListEntries')
    .MTAppListing(freedrinkPlanOption)
    .on('change.MTAppListing', function(){
      $(this).trigger('showListEntries');
    });

  // 選択済み飲み放題
  var freedrinkPlanSelectOption = $.extend({}, listingBaseOption, {});
  freedrinkPlanSelectOption.jsontable.listingCheckboxType = 'radio';
  var $customfield_freedrink_plan_select = $('#customfield_freedrink_plan_select');
  $customfield_freedrink_plan_select
    .MTAppShowListEntries({
      api: mtappVars.DataAPI,
      siteId: mtappVars.cotedazur.freedrinkBlogId
    });
  $customfield_freedrink_plan_select
    .trigger('showListEntries')
    .MTAppListing(freedrinkPlanSelectOption)
    .on('change.MTAppListing', function(){
      $(this).trigger('showListEntries');
    });

})(jQuery);
/*  プランマトリクス管理：コース  */

/* ==================================================
    プランマトリクス管理：飲み放題
================================================== */
(function($){

  if (mtappVars.blog_id != <mt:Var name="blog_id_freedrink" />) {
    return false;
  }

  // フィールドの並べ替え
  var sort = [
    'title',
    'customfield_cf_freedrink_price',
    'customfield_cf_freedrink_week',
    'customfield_cf_freedrink_img'
  ];
  $.MTAppFieldSort({
    sort: sort.join(','),
    otherFieldHide: true
  });

  // カテゴリ選択をラジをボタンにする
  mtappVars.cotedazur.radioCategory();

})(jQuery);
/*  プランマトリクス管理：飲み放題  */

/* ==================================================
    プランマトリクス管理：オプション
================================================== */
(function($){

  if (mtappVars.blog_id != <mt:Var name="blog_id_option" />) {
    return false;
  }

  // フィールドの並べ替え
  $.MTAppFieldSort({
    sort: 'title',
    otherFieldHide: true
  });

  // カテゴリ選択をラジをボタンにする
  mtappVars.cotedazur.radioCategory();

})(jQuery);
/*  プランマトリクス管理：オプション  */

/* ==================================================
    店舗管理
================================================== */
(function($){

  if (mtappVars.blog_id != <mt:Var name="blog_id_store" />) {
    return false;
  }

  // フィールドの並べ替え
  var sort = [
    'title',
    'customfield_cf_store_code',
    'customfield_cf_store_tel',
    'customfield_cf_store_web_rsv_open',
    'customfield_cf_store_web_rsv_end',
    'category',
    'customfield_cf_store_url',
    'customfield_cf_store_outer_service',
    'customfield_cf_store_prefecture',
    'customfield_cf_course'
  ];
  $.MTAppFieldSort({
    sort: sort.join(','),
    otherFieldHide: true
  });

  // 外部サービス
  $('#customfield_cf_store_url').MTAppJSONTable({
    inputType: 'input',
    header: {title: 'タイトル', url: 'URL'},
    headerPosition: 'top',
    headerOrder: ['title','url'],
    footer: false,
    edit: true,
    add: true,
    clear: true,
    sortable: true,
    debug: false
  });

  // コースカテゴリ取得  
  //cscate.jsをロード
  $.ajax({
	url: "../mt-static/support/mtappjquery/cscate.js",
	dataType: "script"
  })
  .done(function( data, textStatus, jqXHR ) {
	//処理実行
	$('#customfield_cf_course').MTAppMultiForm({
		type: 'select',
		items: val_cscate
    });
  });

  // カテゴリ選択をラジをボタンにする
  $.MTAppOtherTypeCategories({
    type: 'select',
    label: '都道府県',
    notSelectedText: '未選択',
    debug: false
  });

})(jQuery);
/*  店舗管理  */

/* ==================================================
    Webフォーム管理
================================================== */
(function($){

  if (mtappVars.blog_id != <mt:Var name="blog_id_form" />) {
    return false;
  }

  // フィールドの並べ替え
  var sort = [
    'title',
    'customfield_cf_step1_bottom',
    'customfield_cf_step1_bottom',
    'customfield_cf_step1_modal_area_bottom',
    'customfield_cf_step1_modal_pref_bottom',
    'customfield_cf_step1_modal_store_upper',
    'customfield_cf_step1_modal_store_bottom',
    'customfield_cf_step2_bottom',
    'customfield_cf_step2_modal_cal_bottom',
    'customfield_cf_step2_modal_sttime_bottom',
    'customfield_cf_step2_modal_time_bottom',
    'customfield_cf_step2_modal_num_bottom',
    'customfield_cf_step3_bottom',
    'customfield_cf_step3_modal_course_upper',
    'customfield_cf_step3_modal_course_bottom',
    'customfield_cf_step3_modal_discount_bottom',
    'customfield_cf_step3_modal_freedrink_bottom',
    'customfield_cf_step3_modal_birthday_bottom',
    'customfield_cf_step3_modal_oiwai_bottom',
    'customfield_cf_step3_modal_option_middle',
    'customfield_cf_step3_modal_option_bottom',
    'customfield_cf_step4_name',
    'customfield_cf_step4_company',
    'customfield_cf_step4_phone',
    'customfield_cf_step4_phone_bottom',
    'customfield_cf_step4_mail',
    'customfield_cf_step4_mail_bottom',
    'customfield_cf_step4_notes',
    'customfield_cf_step4_mailmag_bottom',
    'customfield_cf_step4_policy_upper',
    'customfield_cf_step4_policy_bottom',
    'customfield_cf_step4_agree_bottom',
    'customfield_cf_step4_confirm_bottom',
    'customfield_cf_complete_text'
  ];
  $.MTAppFieldSort({
    sort: sort.join(','),
    otherFieldHide: true
  });

  // カテゴリ選択をラジをボタンにする
  mtappVars.cotedazur.radioCategory();

})(jQuery);
/*  Webフォーム管理  */