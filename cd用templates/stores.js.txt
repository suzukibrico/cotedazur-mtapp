<mt:Ignore>==================================================
Template Name : stores.js
Template Type : index / blog
Current  Site : コートダジュールHP > 店舗管理
Template Note :
==================================================</mt:Ignore>
<mt:Unless name="compress" compress="3" trim="1">
<mt:Include module="config" parent="1" />
<MTsetVar name="count" value="1">
var stores =
{
<mt:SubCategories top="1">
<mt:If tag="CategoryCount">
<mt:CategoryLabel setvar="cat_label" />
  <mt:If name="count" gt="1">,</mt:If>
  <MTSetVar name="count" value="1" op="+">
  "<mt:Var name="cat_label" />":
  [
    <mt:GroupEntries group="$cat_label">
      <mt:If tag="cf_store_url">
        <mt:cf_store_url json_decode="1" setvar="url_hash" />
      </mt:If>
    {
      "store_code": "<mt:cf_store_code escape="html" />",
      "store_name": "<mt:EntryTitle escape="html" />",
      "tel": "<mt:cf_store_tel />",
      "url": <mt:If name="url_hash"><mt:Var name="url_hash" key="items" to_json="1" /><mt:Else>[]</mt:If>,
      "web_rsv_end": "<mt:cf_store_web_rsv_end format="%Y/%m/%d %H:%M" _default="2040/12/31 00:00" />",
      "web_rsv_open": "<mt:cf_store_web_rsv_open format="%Y/%m/%d %H:%M" _default="2000/01/01 00:00" />",
      "outer_service": "<mt:cf_store_outer_service />"
    }<mt:GroupEntriesFooter><mt:Else>,</mt:GroupEntriesFooter>
    </mt:GroupEntries>
  ]
</mt:if>
</mt:SubCategories>
};
</mt:Unless>
