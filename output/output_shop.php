<?php
	include_once('func.common.inc');
	include_once('func.field.inc');
	set_include_path('./Classes/');
	include_once('PHPExcel.php');
	$rtnHtml = "";
	
	$data = $_POST;
	$cntCreate = 0;	
	if(is_file("./excels/shoplist.xlsx")){

		// 既存ファイルの読み込みの場合
		$objPHPExcel = PHPExcel_IOFactory::load("./excels/shoplist.xlsx");
	//if (is_object($objPHPExcel)){
		// 0番目のシートをアクティブにする（シートは左から順に、0、1，2・・・）
		$objPHPExcel->setActiveSheetIndex(0);
		// アクティブにしたシートの情報を取得
		$objSheet = $objPHPExcel->getActiveSheet();
		//テンプレート取得（プランリスト）
		$plantemplate = setTemplate('/var/www/sub/iprimo/html/output/shop.inc');
		$update = date('m/d/Y h:i:s A');

		//loop
		for ($i = 2; $i < 200; $i++){
			$shopcd = $objSheet->getCellByColumnAndRow( 0, $i )->getValue();
			if (strlen($shopcd) > 0){
			//ショップコードが記入されていたら残りも処理
				$cntCreate++;
				
				$shopname	= $objSheet->getCellByColumnAndRow( 1, $i )->getValue();
				$pref		= $objSheet->getCellByColumnAndRow( 3, $i )->getValue();
				$tel		= $objSheet->getCellByColumnAndRow( 4, $i )->getValue();
				$serv		= $objSheet->getCellByColumnAndRow( 5, $i )->getValue();
				$url1		= $objSheet->getCellByColumnAndRow( 6, $i )->getValue();
				$url2		= $objSheet->getCellByColumnAndRow( 7, $i )->getValue();
				$startdate	= $objSheet->getCellByColumnAndRow( 8, $i )->getValue();
				$enddate	= $objSheet->getCellByColumnAndRow( 9, $i )->getValue();
				
				//url 加工
				if ((strlen($url1) > 0)||(strlen($url2) > 0)){
					$urlstring_s = '{"items":[';
					$urlstring_contents = '';
					$urlstring_e = ']}';
				}else{
					$urlstring_s = '';
					$urlstring_e = '';
					$urlstring_contents = '';
				}
				if (strlen($url1) > 0){
					if (strstr($url1,'hotpepper')){
						$sitename1 = 'ホットペッパーグルメ';
					}else if (strstr($url1,'gnavi')){
						$sitename1 = 'ぐるなび';
					}else{
						$sitename1 = $shopname;
					}
					$urlstring_contents = '{"title":"'.$sitename1.'","url":"'.$url1.'"}';
				}
				if (strlen($url2) > 0){
					if (strstr($url2,'hotpepper')){
						$sitename2 = 'ホットペッパーグルメ';
					}else if (strstr($url2,'gnavi')){
						$sitename2 = 'ぐるなび';
					}else{
						$sitename2 = $shopname;
					}
					if (strlen($urlstring_contents) > 0){
						$urlstring_contents .= ',';
					}
					$urlstring_contents .= '{"title":"'.$sitename2.'","url":"'.$url2.'"}';
				}
				$urlstring = $urlstring_s . $urlstring_contents . $urlstring_e;
				
				$replaces = array (
					'<# TITLE #>' 	=> $shopname,
					'<# PREF #>'	 	=> $pref,
					'<# UPDATE #>'	 	=> $update,
					'<# SERVICE #>'	 	=> $serv,
					'<# URL #>' 		=> $urlstring,
					'<# STARTDATE #>'	 => ((strlen($startdate) > 0)?date("m/d/Y",strtotime($startdate)).' 12:00:00 AM':''),
					'<# ENDDATE #>' 	=>  ((strlen($enddate) > 0)?date("m/d/Y",strtotime($enddate)).' 12:00:00 AM':''),
					'<# CODE #>' 	=> $shopcd,
					'<# TEL #>' 	=> $tel
				);
				$rtnBody = getReplaceString($plantemplate,$replaces);
				$rtnHtml = $rtnHtml ."\r".$rtnBody;
			}
		}
	
		//インポートファイル作成
		$file = 'import'.date(YmdHis).'.txt';
		$srcfile = '/var/www/sub/iprimo/html/output/cotedazur/'.$file;
		//前回一時ファイルを削除＆新規作成
		if(is_file($srcfile)){
			unlink($srcfile);
		}
		//ファイルに書き込み
		$fp = fopen($srcfile, 'a+');
		if($fp != FALSE){
			if(fwrite($fp, $rtnHtml) != FALSE){
			}
			fclose($fp);
		}
	}
	$rtnHtml = "";
	$planlisttag = "";
	//////////////////////////////////////////////////////////////////////////////
	// メール内容取得
	// 引数:
	// 戻値:結果(true Or false)
	function setTemplate ($p_filename) {
		$file 	= $p_filename;
		if (!is_readable($file)) {
			return false;
		}

		$lines 						= file($file);

		$body	= implode('', $lines);
		$template	= str_replace("\r", "", $body);
		
		return $template;

	}

	//////////////////////////////////////////////////////////////////////////////
	// メール内容取得
	// 引数:
	// 戻値:結果(true Or false)
	function getReplaceString ($p_body,$p_replaces) {
		$body = $p_body;

		if (count($p_replaces) > 0) {
			foreach ($p_replaces as $key => $val) {
				$body = str_replace($key, str_replace("\r", "", $val), $body);
			}
		}
		return $body;
	}

?>
<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ja"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
</head>
<body>
<form action="output.php" method="post">
<br>
<h1>結果</h1>
<br>
<?=$cntCreate?>店作成しました。
<br>
<Br>
<br>
<a href="shoplist.php">←戻る</a>
</form>
</body>
</html>