<?php

	/***************************************************************************
	 * Name 		:func.field.inc
	 * Description 		:フィールド生成関数
	 * Include		:
	 * Trigger		:
	 * Create		:2009/07/02 Brico suzuki
	 * LastModify	:
	 *
	 *
	 *
	 **************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 日付セレクトボックス(年)
	// 引数:フィールド名称
	// 		現在の値
	// 		開始年
	// 		終了年
	// 		並び順(asc || desc)
	// 戻値:HTML
	function getYearSel ($p_name, $p_val = null, $p_start = null, $p_end = null, $p_order = 'desc', $p_noselect = '----') {
		$start 	= $p_start;
		$end 	= $p_end;
		if (is_null($start)) {
			$start = 1900;
		}
		if (is_null($end)) {
			$end = date('Y') + 1;
		}
		return getNumSel($p_name, $start, $end, $p_val, $p_order, 4, $p_noselect);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 日付セレクトボックス(月)
	// 引数:フィールド名称
	// 		現在の値
	// 戻値:HTML
	function getMonthSel ($p_name, $p_val = null, $p_digit = 2, $p_noselect = '--') {
		return getNumSel($p_name, 1, 12, $p_val, 'asc', $p_digit,  $p_noselect);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 日付セレクトボックス(日)
	// 引数:フィールド名称
	// 		現在の値
	// 戻値:HTML
	function getDaySel ($p_name, $p_val = null, $p_digit = 2, $p_noselect = '--') {
		return getNumSel($p_name, 1, 31, $p_val, 'asc', $p_digit, $p_noselect);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 日付セレクトボックス(時)
	// 引数:フィールド名称
	// 		現在の値
	// 戻値:HTML
	function getHourSel ($p_name, $p_val = null, $p_digit = 2, $p_noselect = '--') {
		return getNumSel($p_name, 0, 23, $p_val, 'asc', $p_digit, $p_noselect);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 日付セレクトボックス(分)
	// 引数:フィールド名称
	// 		現在の値
	// 戻値:HTML
	function getMinSel ($p_name, $p_val = null, $p_digit = 2, $p_noselect = '--', $p_inc = 1) {
		return getNumSel($p_name, 0, 59, $p_val, 'asc', $p_digit, $p_noselect, $p_inc);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 日付セレクトボックス(秒)
	// 引数:フィールド名称
	// 		現在の値
	// 戻値:HTML
	function getSecSel ($p_name, $p_val = null, $p_digit = 2, $p_noselect = '--') {
		return getNumSel($p_name, 0, 59, $p_val, 'asc', $p_digit, $p_noselect);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 数値セレクトボックス作成
	// 引数:フィールド名称
	// 		開始
	// 		終了
	// 		現在の値
	// 		並び順(asc || desc)
	// 		0埋めする場合は何桁で埋めるか
	// 		未選択時の値
	// 戻値:HTML
	function getNumSel ($p_name, $p_start, $p_end, $p_val = null, $p_order = 'asc', $p_digit = 0, $p_noselect = '----', $p_inc=1) {
		$html 	= array();
		$html[] = '<select name="' . $p_name . '">';
		if (!is_null($p_noselect)){
			$html[] = '<option value="">' . $p_noselect . '</option>';
		}

		switch ($p_order) {
		case 'desc':
			for ($i = $p_end; $i >= $p_start; $i=$i-$p_inc) {
				if (!is_null($p_val)) {
					if ($i == intval($p_val) && strlen($p_val) > 0) {
						$selected = ' selected';
					} else {
						$selected = '';
					}
				}
				if ($p_digit > 0) {
					$format = sprintf('%0' . $p_digit . 'd', $i);
				} else {
					$format = $i;
				}
				$html[] = 	'<option value="' . $format . '"' . $selected . '>' . 
								$format . 
							'</option>';
			}
			break;
		default:
			for ($i = $p_start; $i <= $p_end; $i=$i+$p_inc) {
				if (!is_null($p_val)) {
					if ($i == intval($p_val) && strlen($p_val) > 0) {
						$selected = ' selected';
					} else {
						$selected = '';
					}
				}
				if ($p_digit > 0) {
					$format = sprintf('%0' . $p_digit . 'd', $i);
				} else {
					$format = $i;
				}
				$html[] = 	'<option value="' . $format . '"' . $selected . '>' . 
								$format . 
							'</option>';
			}
			break;
		}

		$html[] = '</select>';

		return implode("\n", $html);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 日付からYYYY/MM/DD HH24:MI:SSセレクトボックス作成
	// 引数:フィールド名称
	// 		現在の値(配列可)
	// 		開始年
	// 		終了年
	// 		年の並び順(asc || desc)
	// 		日付フォーマット(配列)
	// 戻値:HTML
	function getTimeSelect($p_name, $p_date, $p_start = null, $p_end = null, $p_order = 'desc', $p_format = array('/', '/', '&nbsp', ':', ':')) {

		if (strlen($p_date) > 0) {
			if (!is_array($p_date)) {
				$date 		= strtotime($p_date);
				$arrTemp[] 	= date('Y', $date);
				$arrTemp[] 	= date('m', $date);
				$arrTemp[] 	= date('d', $date);
				$arrTemp[] 	= date('H', $date);
				$arrTemp[] 	= date('i', $date);
				$arrTemp[] 	= date('s', $date);
			} else {
				if (strlen(implode('', $p_date)) > 0) {
					$arrTemp = $p_date;
				}
			}
		}
		if (!isset($arrTemp)) {
			$arrTemp[3] = -1;
			$arrTemp[4] = -1;
			$arrTemp[5] = -1;
		}

		$select .= getYearSel($p_name, $arrTemp[0], $p_start, $p_end, $p_order) . $p_format[0];
		$select .= getMonthSel($p_name, $arrTemp[1]) . $p_format[1];
		$select .= getDaySel($p_name, $arrTemp[2]) . $p_format[2];
		$select .= getHourSel($p_name, $arrTemp[3]) . $p_format[3];
		$select .= getMinSel($p_name, $arrTemp[4]) . $p_format[4];
		$select .= getSecSel($p_name, $arrTemp[5]);

		return $select;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 日付からYYYY/MM/DD HH24:MI:SSセレクトボックス作成
	// 引数:フィールド名称
	// 		現在の値(配列可)
	// 		開始年
	// 		終了年
	// 		年の並び順(asc || desc)
	// 		日付フォーマット(配列)
	// 戻値:HTML
	function getTimeSel($p_name, $p_date, $p_format = array('/', '/', '&nbsp', ':', ':')) {

		if (strlen($p_date) > 0) {
			if (!is_array($p_date)) {
				$date 		= strtotime($p_date);
				$arrTemp[] 	= date('Y', $date);
				$arrTemp[] 	= date('m', $date);
				$arrTemp[] 	= date('d', $date);
				$arrTemp[] 	= date('H', $date);
				$arrTemp[] 	= date('i', $date);
				$arrTemp[] 	= date('s', $date);
			} else {
				if (strlen(implode('', $p_date)) > 0) {
					$arrTemp = $p_date;
				}
			}
		}
		if (!isset($arrTemp)) {
			$arrTemp[3] = 0;
			$arrTemp[4] = 0;
		}

		//$select .= getYearSel($p_name, $arrTemp[0], $p_start, $p_end, $p_order) . $p_format[0];
		//$select .= getMonthSel($p_name, $arrTemp[1]) . $p_format[1];
		//$select .= getDaySel($p_name, $arrTemp[2]) . $p_format[2];
		$select .= '<li class="time_margin"> <img src="img/entry/time.gif" alt="時" width="18" height="17" />' . getHourSel($p_name, $arrTemp[3],2,null).'</li>';
		$select .= '<li> <img src="img/entry/minute.gif" alt="分" width="18" height="17" />' . getMinSel($p_name, $arrTemp[4],2,null).'</li>';
		//$select .= getSecSel($p_name, $arrTemp[5]);

		return $select;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
	// 日付からYYYY/MM/DDセレクトボックス作成
	// 引数:フィールド名称
	// 		現在の値(配列可)
	// 		開始年
	// 		終了年
	// 		年の並び順(asc || desc)
	// 戻値:HTML
	function getDateSelect($p_name, $p_date, $p_start = null, $p_end = null, $p_order = 'desc') {
		if (strlen($p_date) > 0) {
			if (!is_array($p_date)) {
				$date 		= strtotime($p_date);
				$arrTemp[] 	= date('Y', $date);
				$arrTemp[] 	= date('m', $date);
				$arrTemp[] 	= date('d', $date);
			} else {
				$arrTemp 	= $p_date;
			}
		}
		$select .=  '&nbsp;'
			 . getYearSel($p_name, $arrTemp[0], $p_start, $p_end, $p_order,'選択') ;
		$select .=  '年&nbsp;'
			 . getMonthSel($p_name, $arrTemp[1],2,'選択') ;
		$select .=   '月&nbsp;'
			 . getDaySel($p_name, $arrTemp[2],2,'選択') . '日&nbsp;';
		return $select;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////
	// 連想配列からチェックボックスを作成
	// 引数:連想配列
	//		フィールド名
	// 		現在の値(配列)
	// 戻値:文字列
	function getCheckBox ($p_values, $p_name, $p_checked) {
		$html = array();
		if (!is_array($p_values)) {
			return '';
		}
		if (!is_array($p_checked)) {
			$checked = array();
		} else {
			$checked = $p_checked;
		}
		foreach($p_values as $key => $val){
			if (in_array(strval($key), $checked)) {
				$option = ' checked';
			}else{
				$option = '';
			}
			$html[] = '<input type="checkbox" name="' . htmlspecialchars($p_name) . '" value="' . htmlspecialchars($key) . '"' . $option . '>' . htmlspecialchars($val);
		}
		return implode("\n", $html);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////
	// 連想配列からチェックボックスを作成
	// 引数:連想配列
	//		フィールド名
	// 		現在の値
	// 		なにも選択されてない場合の値
	// 戻値:文字列
	function getRadioBtn ($p_values, $p_name, $p_checked, $p_nocheck = null) {
		$html = array();
		if (!is_null($p_nocheck)) {
			if (strlen($p_checked) > 0) {
				$option = ' checked';
			} else {
				$option = '';
			}
			$html[] = '<input type="radio" name="' . htmlspecialchars($p_name) . '" value=""' . $option . ' id="'.$p_name.'_no">&nbsp;&nbsp;&nbsp;' . '<span onclick="setRadio('."'".$p_name."_no'".');">'.htmlspecialchars($p_nocheck).'</span>';
		}
		foreach($p_values as $key => $val){
			if (strval($key) == $p_checked) {
				$option = ' checked';
			}else{
				$option = '';
			}
			$html[] = '<input type="radio" name="' . htmlspecialchars($p_name) . '" value="' . htmlspecialchars($key) . '"' . $option . ' id="'.$p_name.'_'.$key.'">&nbsp;&nbsp;&nbsp;' . '<span onclick="setRadio('."'".$p_name.'_'.$key."'".');">'. htmlspecialchars($val).'</span>';
		}
		return implode("\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $html);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 連想配列からセレクトボックスを作成
	// 引数:連想配列
	//		フィールド名
	// 		現在の値
	// 		なにも選択されてない場合の値
	// 		オプション
	// 戻値:文字列
	function getSelectBox ($p_values, $p_name, $p_selected, $p_noselect = '-------', $p_opt = null) {
		if (!is_null($p_opt)) {
			$opt = $p_opt;
		} else {
			$opt = '';
		}

		$html[] = '<select name="' . htmlspecialchars($p_name) . '"' . $opt . '>';
		if (strlen($p_noselect) > 0) {
			$html[] = '<option value="">' . htmlspecialchars($p_noselect) . '</option>';
		}
		foreach($p_values as $key => $val){
			if (is_array($val)){
				$val2 = $val[0];
			}else{
				$val2 = $val;
			}
		
			if (strval($key) == $p_selected) {
				$option = ' selected';
			}else{
				$option = '';
			}
			$html[] = '<option value="' . htmlspecialchars($key) . '"' . $option . '>' . htmlspecialchars($val2) . '</option>';
		}
		$html[] = '</select>';
		return implode("\n", $html);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 都道府県SELECTボックスを取得
	// 引数:フィールド名
	// 		現在の値
	// 		なにも選択されてない場合の値
	// 		オプション
	// 戻値:HTML
	function getPrefucture ($p_name, $p_selected, $p_noselect = '都道府県の選択', $p_opt = null) {
		if (!is_null($p_opt)) {
			$p_opt = $p_opt;
		} else {
			$p_opt = '';
		}

		$html[] = '<select name="' . htmlspecialchars($p_name) . '"' . $opt . '>';
		if (strlen($p_noselect) > 0) {
			$html[] = '<option value="">' . htmlspecialchars($p_noselect) . '</option>';
		}

		foreach ($GLOBALS['_ARA_AREA'] as $key1 => $val1) {
			if (strlen($val1[0]) > 0) {
				$html[] = '<optgroup label="' . htmlspecialchars($val1[0]) . '">';
			}
			foreach($val1[1] as $key => $val){
				if (strval($key) == $p_selected) {
					$option = ' selected';
				}else{
					$option = '';
				}
				$html[] = '<option value="' . htmlspecialchars($key) . '"' . $option . '>' . htmlspecialchars($val) . '</option>';
			}
			$html[] = '</optgroup>';
		}
		$html[] = '</select>';
		return implode("\n", $html);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 引数の配列からhidden作成
	// 引数:配列
	// 		hiddenを作成しない要素
	// 更新:2005/08/26 Yamada 連想配列形式に対応
	// 戻値:HTML
	function array2hidden($p_ary, $p_omit=array()){
		$ret = array();
		if(is_array($p_ary)){
			foreach($p_ary as $key => $value){
				if (!in_array($key, $p_omit)) {
					if (is_array($value)) {
						if (array_keys($value) === range(0, count($value) - 1)) {
							// 通常配列
							foreach($value as $val){
								$ret[] = '<input type="hidden" name="' . $key . '[]" value="' . htmlspecialchars($val) . '">';
							}
						} else {
							// 連想配列
							foreach($value as $key2 => $val){
								$ret[] = '<input type="hidden" name="' . $key . '[' . $key2 . ']" value="' . htmlspecialchars($val) . '">';
							}
						}
					} else {
						$ret[] = '<input type="hidden" name="' . $key . '" value="' . htmlspecialchars($value) . '">';
					}
				}
			}
		}
		return implode("\n",$ret);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
	// 引数の配列からhidden作成
	// 引数:配列
	// 		hiddenを作成しない要素
	// 更新:2005/08/26 Yamada 連想配列形式に対応
	// 戻値:HTML
	function array2hiddenEnc($p_ary, $p_omit=array()){
		$ret = array();
		if(is_array($p_ary)){
			foreach($p_ary as $key => $value){
				if (!in_array($key, $p_omit)) {
					if (is_array($value)) {
						if (array_keys($value) === range(0, count($value) - 1)) {
							// 通常配列
							foreach($value as $val){
								$ret[] = '<input type="hidden" name="' . $key . '[]" value="' . mbConv(htmlspecialchars($val)) . '">';
							}
						} else {
							// 連想配列
							foreach($value as $key2 => $val){
								$ret[] = '<input type="hidden" name="' . $key . '[' . $key2 . ']" value="' . mbConv(htmlspecialchars($val)) . '">';
							}
						}
					} else {
						$ret[] = '<input type="hidden" name="' . $key . '" value="' . mbConv(htmlspecialchars($value)) . '">';
					}
				}
			}
		}
		return implode("\n",$ret);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 配列のコードを配列の値にして返す
	// 引数:コード => 値配列
	// 		コード配列
	// 戻値:配列
	function getArrayValue ($p_values, $p_keys) {
		$returns = array();
		if (!is_array($p_keys) || count($p_keys) == 0) {
			return $returns;
		}
		foreach ($p_keys as $key) {
			if (array_key_exists(strval($key), $p_values)) {
				array_push($returns, $p_values[$key]);
			}
		}
		return $returns;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// 文字列を文字位置で分割して返す
	// 引数:分割文字位置
	// 		文字列
	// 戻値:配列
	function explodePos ($p_pos, $p_value) {
		$returns = array();
		if (strlen($p_value) < $p_pos) {
			return $returns;
		}
		array_push($returns, substr($p_value, 0, $p_pos));
		array_push($returns, substr($p_value, $p_pos, strlen($p_value)));
		return $returns;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// カラーチェックボックスを取得
	// 引数:フィールド名
	// 		現在の値(配列)
	// 戻値:HTML
	function getColorCheck ($p_name, $p_values) {
		$colors1 	= array();
		$colors2 	= array();
		$tag 		= 	'<td class="text12"><input type="checkbox" name="%s" value="%s"%s></td>' . 
						'<td class="text12">%s</td>';
		$spacer 	= '<td><img src="/img/spacer.gif" width="8" height="10"></td>';
		$html 		= '<table border="0" cellspacing="1" cellpadding="0"><tr>%s</tr><tr>%s</tr></table>';

		foreach ($GLOBALS['_UNIFORM_IMG_1'] as $key => $val) {
			if (is_array($p_values) && in_array($key, $p_values)) {
				$checked = ' checked';
			} else {
				$checked = '';
			}
			$colors1[] = sprintf(
							$tag,
							htmlspecialchars($p_name),
							htmlspecialchars($key),
							$checked,
							$val
						);
		}
		foreach ($GLOBALS['_UNIFORM_IMG_2'] as $key => $val) {
			if (is_array($p_values) && in_array($key, $p_values)) {
				$checked = ' checked';
			} else {
				$checked = '';
			}
			$colors2[] = sprintf(
							$tag,
							htmlspecialchars($p_name),
							htmlspecialchars($key),
							$checked,
							$val
						);
		}
		return sprintf($html, implode($spacer, $colors1), implode($spacer, $colors2));
	}
?>